package com.example.drawermenuapp.ui.adapter

import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.drawermenuapp.CustomOnClick
import com.example.drawermenuapp.MainActivity
import com.example.drawermenuapp.R
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.menu_item_layout.view.*

class MenuRecyclerViewAdapter(
    private val items: ArrayList<MenuItemsModel>,
    private val activity: MainActivity, val onClick: CustomOnClick
) :
    RecyclerView.Adapter<MenuRecyclerViewAdapter.ViewHolder>() {
    private var currentPosition = -1

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {

        fun onBind() {
            itemView.setOnClickListener(this)
            val menuItem = items[adapterPosition]
            itemView.iconImageView.setImageResource(menuItem.icon)
            itemView.titleTextView.text = menuItem.title
            changeColor()
            itemView.setOnClickListener {
                currentPosition = adapterPosition
                notifyDataSetChanged()

            }

        }

        fun changeColor() {
            if (currentPosition == adapterPosition) {
                itemView.setBackgroundColor(
                    ContextCompat.getColor(
                        activity,
                        R.color.colorLightGrey
                    )
                )

            } else {
                itemView.setBackgroundColor(ContextCompat.getColor(activity, android.R.color.white))
            }

        }

        override fun onClick(v: View?) {
            onClick.onClick(adapterPosition)
        }


    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.menu_item_layout, parent, false
        )
    )

    override fun getItemCount(): Int = items.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()

    }
}