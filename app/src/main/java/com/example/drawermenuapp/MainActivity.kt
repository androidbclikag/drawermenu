package com.example.drawermenuapp

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.drawermenuapp.ui.adapter.MenuItemsModel
import com.example.drawermenuapp.ui.adapter.MenuRecyclerViewAdapter
import com.example.drawermenuapp.ui.gallery.GalleryFragment
import com.example.drawermenuapp.ui.home.HomeFragment
import com.example.drawermenuapp.ui.slideshow.SlideshowFragment
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import java.text.FieldPosition
import kotlin.properties.Delegates


class MainActivity : AppCompatActivity(){

    private lateinit var appBarConfiguration: AppBarConfiguration
    private var items = ArrayList<MenuItemsModel>()
    private var customPosition = -1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        menuItemsInit()

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }

    private fun init() {
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }


    private fun menuItemsInit() {
        giveMenuItems()

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = MenuRecyclerViewAdapter(items, this, object : CustomOnClick {
            override fun onClick(position: Int) {
                customPosition = position
//                when(items[position]){
//                    items[0] -> nav_host_fragment.findNavController().navigate(R.id.nav_home)
//                    items[1] -> nav_host_fragment.findNavController().navigate(R.id.nav_gallery)
//                    items[2] -> nav_host_fragment.findNavController().navigate(R.id.nav_slideshow)

            }
        }

        )
    }

//    private fun changeFragment() {
//        val fragments = mutableListOf<Fragment>()
//        fragments.add(HomeFragment())
//        fragments.add(GalleryFragment())
//        fragments.add(SlideshowFragment())
//
//
//    }

    private fun giveMenuItems() {
        val itemZero = MenuItemsModel(R.drawable.ic_menu_camera, "Home")
        val item = MenuItemsModel(R.drawable.ic_menu_gallery, "Gallery")
        val itemTwo = MenuItemsModel(R.drawable.ic_menu_slideshow, "SlideShow")
        items.add(itemZero)
        items.add(item)
        items.add(itemTwo)
    }

//    override fun onNavigationItemSelected(item: MenuItem) {
//        when (items[customPosition]) {
//            items[0] -> nav_host_fragment.findNavController().navigate(R.id.nav_home)
//            items[1] -> nav_host_fragment.findNavController().navigate(R.id.nav_gallery)
//            items[2] -> nav_host_fragment.findNavController().navigate(R.id.nav_slideshow)
//        } true
//
//    }
}

